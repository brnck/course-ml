<?php

use brnck\DatabaseManager;
use brnck\Entity\Request;
use brnck\Entity\Result;
use brnck\Services\ClassificationProcessor;
use brnck\Services\ClusteringProcessor;
use brnck\Services\DataSelector;
use brnck\Services\DbScanAlgorithm;
use brnck\Services\KmeansAlgorithm;
use brnck\Services\KNearestNeighborsAlgorithm;
use brnck\Services\RequestProcessor;
use brnck\Services\Timer;
use Phpml\Classification\KNearestNeighbors;
use Phpml\Clustering\DBSCAN;
use Phpml\Clustering\KMeans;

require_once 'vendor/autoload.php';

if (isset($_GET['request'])) {

    try {
        /** @var Request $request */
        $request = (new RequestProcessor())->processRequest($_GET);

        $dataSelector = new DataSelector(new DatabaseManager());

        $clusteringProcessor = (new ClusteringProcessor(new Timer()))
            ->addProcessor(
                new DbScanAlgorithm(new DBSCAN($request->getEpsilon(), $request->getMinSamples())),
                'db_scan'
            )
            ->addProcessor(new KmeansAlgorithm(new KMeans($request->getClusters())), 'k_means');

        $classificationProcessor = (new ClassificationProcessor(new Timer()))
            ->addProcessor(new KNearestNeighborsAlgorithm(new KNearestNeighbors(2)), 'k_nearest_neighbors')
        ;

        switch ($request->getType()) {
            case Request::TYPE_CLUSTERING:
                /** @var Result $result */
                $result = $clusteringProcessor->process($request, $dataSelector->getResults($request));

                include_once 'templates/clustering.php';
                break;
            case Request::TYPE_CLASSIFICATION:
                $request->setMethod('k_nearest_neighbors');
                $result = $classificationProcessor->process($request, $dataSelector->getResultsForClassification($request));

                include_once 'templates/classification.php';
                break;
        }

    } catch (Exception $exception) {
        die($exception->getMessage());
    }
} else {
    include_once 'templates/index.php';
}