<?php

namespace brnck;

use brnck\Entity\Request;
use Faker\Factory;
use PDO;
use Phpml\Exception\DatasetException;

class DatabaseManager
{
    const LINES = 20000;
    const COLUMNS = [
        'gender' => 'Lytis',
        'age' => 'Amžius',
        'equipment' => 'Ar ėmė įrangą?',
        'time_spend' => 'Praleido laiko',
        'track' => 'Trasa',
        'user' => 'Vartotojas',
    ];

    const FIELDS = [
        'gender' => [
            0 => 'Vyras',
            1 => 'Moteris',
        ],
        'equipment' => [
            0 => 'Ne',
            1 => 'Taip',
        ],
    ];

    private $connection;
    private $faker;

    public function __construct()
    {
        $this->connection = DatabaseConnector::create();
        $this->faker = Factory::create();
    }

    public function generateData()
    {
        for($i = 0; $i < self::LINES; $i++) {
            $sql = sprintf(
                'INSERT INTO `customers` VALUES (%d, %d, %d, %d, %d, %d)',
                $this->faker->boolean(),
                $this->faker->numberBetween(10, 60),
                $this->faker->boolean(70),
                $this->faker->numberBetween(1, 10),
                $this->faker->numberBetween(1, 3),
                $this->faker->numberBetween(1, 1000)
            );

            $result = $this->connection->getConnection()->exec($sql);

            echo PHP_EOL . $result;
        }

        echo PHP_EOL;
    }

    /**
     * @param Request $request
     * @return array
     * @throws DatasetException
     */
    public function getFromCustomersSpecifiedKeys(Request $request)
    {
        $sql = 'SELECT ';
        $sql .= implode(', ', $request->getColumns());
        $sql .= ' FROM `customers`';

        if ($request->getDbQueryLimit() !== null) {
            $sql .= ' LIMIT '. $request->getDbQueryLimit();
        }

        $result = $this->connection->getConnection()->query($sql)->fetchAll(PDO::FETCH_NUM);

        if (!empty($result)) {
            return $result;
        }

        throw new DatasetException('Bad query. Results returned 0');
    }

    public static function mapColumnToValue($value)
    {
        return self::COLUMNS[$value];
    }

    public static function mapFieldToValue($key, $valueKey)
    {
        if (!isset(self::FIELDS[$key])) {
            return $valueKey;
        }

        return self::FIELDS[$key][$valueKey];
    }
}