<?php

namespace brnck;

use PDO;

class DatabaseConnector
{
    private $connection;
    private static $databaseConnector;

    /**
     * @return PDO
     */
    public function getConnection()
    {
        return $this->connect();
    }

    /**
     * @return bool
     */
    public function close()
    {
        $this->connection = null;

        return true;
    }
    
    /**
     * @return PDO
     */
    private function connect()
    {
        if ($this->connection === null) {
            $this->connection = new PDO('mysql:host=127.0.0.1;dbname=clustering', 'root', '');
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        
        return $this->connection;
    }

    public static function create()
    {
        if (self::$databaseConnector === null) {
            self::$databaseConnector = new self;
        }
        
        return self::$databaseConnector;
    }
}