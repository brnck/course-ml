<?php

namespace brnck\Services;

use brnck\Entity\Request;
use InvalidArgumentException;
use Phpml\Classification\KNearestNeighbors;

class KNearestNeighborsAlgorithm implements AlgorithmInterface
{
    private $KNearestNeighbor;

    public function __construct(KNearestNeighbors $KNearestNeighbor)
    {
        $this->KNearestNeighbor = $KNearestNeighbor;
    }

    public function process(array $data, Request $request)
    {
        if ($request->getAgeOrTime() === null) {
            throw new InvalidArgumentException('Please enter age or time');
        }

        $labels = [];
        $samples = [];
        foreach ($data as $value) {
            $samples[][] = array_values($value)[0];
            $labels[] = array_keys($value)[0];
        }

        $this->KNearestNeighbor->train($samples, $labels);

        return [
            $this->KNearestNeighbor->predict([$request->getAgeOrTime()])
        ];
    }
}