<?php

namespace brnck\Services;

use brnck\Entity\Request;
use InvalidArgumentException;

class RequestProcessor
{
    public function processRequest(array $get)
    {
        if (!isset($get['type']) || !isset($get['columns'])) {
            throw new InvalidArgumentException('No methods selected');
        }

        $request = (new Request())
            ->setColumns($get['columns'])
            ->setMethod($get['method'])
            ->setType($get['type'])
        ;

        if (isset($get['ageOrTime'])) {
            $request->setAgeOrTime($get['ageOrTime']);
        }
        if (isset($get['limit'])) {
            $request->setDbQueryLimit($get['limit']);
        }
        if (isset($get['epsilon'])) {
            $request->setEpsilon($get['epsilon']);
        }
        if (isset($get['min_samples'])) {
            $request->setMinSamples($get['min_samples']);
        }
        if (isset($get['clusters'])) {
            $request->setClusters($get['clusters']);
        }

        return $request;
    }
}