<?php

namespace brnck\Services;

use brnck\DatabaseManager;
use brnck\Entity\Request;
use InvalidArgumentException;

class DataSelector
{
    private $databaseManager;

    public function __construct(DatabaseManager $manager)
    {
        $this->databaseManager = $manager;
    }

    /**
     * @param Request $request
     * @return array
     */
    public function getResults(Request $request)
    {
        return $this->databaseManager->getFromCustomersSpecifiedKeys($request);
    }

    public function getResultsForClassification(Request $request)
    {
        $results = $this->flattenArray($this->databaseManager->getFromCustomersSpecifiedKeys($request));

        if ($request->getColumns()[0] === Request::TYPE_AGE) {
            return $this->processAgeResults($results);
        }
        if ($request->getColumns()[0] === Request::TYPE_TIME) {
            return $this->processTimeResults($results);
        }

        throw new InvalidArgumentException('No method specified');
    }

    private function processAgeResults(array $results)
    {
        $dataSet = [];
        foreach (array_values($results) as $result) {
            if ($result < 19) {
                $dataSet[]['paauglys'] = $result;
            }
            if ($result >= 19 && $result < 55) {
                $dataSet[]['suauges'] = $result;
            }
            if ($result >= 55) {
                $dataSet[]['pensininkas'] = $result;
            }
        }

        return $dataSet;
    }

    private function processTimeResults(array $results)
    {
        $dataSet = [];
        foreach ($results as $result) {
            if ($result < 4) {
                $dataSet[]['trumpai'] = $result;
            }
            if ($result >= 4 && $result < 8) {
                $dataSet[]['vidutiniskai'] = $result;
            }
            if ($result >= 8) {
                $dataSet[]['ilgai'] = $result;
            }
        }

        return $dataSet;
    }

    private function flattenArray(array $results)
    {
        $return = [];
        foreach ($results as $key => $value) {
            if (is_array($value)){
                $return = array_merge($return, $this->flattenArray($value));
            } else {
                $return[$key] = (int) $value;
            }
        }

        return $return;
    }
}