<?php

namespace brnck\Services;

use RuntimeException;

class Timer
{
    /**
     * @var float
     */
    private $timeStart;
    /**
     * @var float
     */
    private $timeEnd;
    /**
     * @var float
     */
    private $timeDiff;

    public function __construct()
    {
        $this->timeStart = 0;
    }

    public function start()
    {
        $this->timeStart = microtime(true);
    }

    public function end()
    {
        $this->timeEnd = microtime(true);
    }

    public function timeDiff()
    {
        if ($this->timeStart !== 0 && $this->timeEnd !== null) {
            return $this->timeEnd - $this->timeStart;
        }

        throw new RuntimeException('Timer was not started ir it was not ended');
    }
}