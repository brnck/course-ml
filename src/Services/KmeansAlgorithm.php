<?php

namespace brnck\Services;

use brnck\Entity\Request;
use Phpml\Clustering\KMeans;

class KmeansAlgorithm implements AlgorithmInterface
{
    private $KMeans;

    public function __construct(KMeans $KMeans)
    {
        $this->KMeans = $KMeans;
    }

    public function process(array $data, Request $request)
    {
        return $this->KMeans->cluster($data);
    }
}