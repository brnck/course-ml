<?php

namespace brnck\Services;

use brnck\Entity\Request;
use brnck\Entity\Result;

class ClusteringProcessor
{
    /**
     * @var AlgorithmInterface[]
     */
    private $processors;
    private $timer;

    public function __construct(Timer $timer)
    {
        $this->timer = $timer;
    }

    /**
     * @param AlgorithmInterface $processor
     * @param string $key
     * @return $this
     */
    public function addProcessor(AlgorithmInterface $processor, $key)
    {
        $this->processors[$key] = $processor;

        return $this;
    }

    /**
     * @param Request $request
     * @param array $data
     * 
     * @return Result
     */
    public function process(Request $request, array $data)
    {
        $result = new Result();

        if (isset($this->processors[$request->getMethod()])) {
            $this->timer->start();
            $clusters = $this->processors[$request->getMethod()]->process($data, $request);
            $this->timer->end();

            $result->setResults($clusters);
            $result->setDataCount(count($data));
            $result->setTimeTook(round($this->timer->timeDiff(), 3));
        }

        return $result;
    }
}