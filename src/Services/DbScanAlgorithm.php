<?php

namespace brnck\Services;

use brnck\Entity\Request;
use Phpml\Clustering\DBSCAN;

class DbScanAlgorithm implements AlgorithmInterface
{
    private $dbScan;

    public function __construct(DBSCAN $dbScan)
    {
        $this->dbScan = $dbScan;
    }
    
    public function process(array $data, Request $request)
    {
        return $this->dbScan->cluster($data);
    }
}