<?php

namespace brnck\Services;

use brnck\Entity\Request;

interface AlgorithmInterface
{
    public function process(array $data, Request $request);
}