<?php

namespace brnck\Entity;

class Result
{
    /**
     * @var array
     */
    private $results;
    /**
     * @var float
     */
    private $timeTook;

    /**
     * @var int
     */
    private $dataCount;

    /**
     * @return int
     */
    public function getDataCount()
    {
        return $this->dataCount;
    }

    /**
     * @param int $dataCount
     * @return $this
     */
    public function setDataCount(int $dataCount)
    {
        $this->dataCount = $dataCount;
        return $this;
    }

    /**
     * @return float
     */
    public function getTimeTook()
    {
        return $this->timeTook;
    }

    /**
     * @param float $timeTook
     * @return $this
     */
    public function setTimeTook(float $timeTook)
    {
        $this->timeTook = $timeTook;

        return $this;
    }

    public function __construct()
    {
        $this->results = [];
    }

    public function addResult(array $result)
    {
        $this->results[] = $result;
    }

    public function getResults()
    {
        return $this->results;
    }

    /**
     * @param array $results
     *
     * @return $this
     */
    public function setResults($results)
    {
        $this->results = $results;

        return $this;
    }
}