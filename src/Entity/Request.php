<?php

namespace brnck\Entity;

use InvalidArgumentException;

class Request
{
    const TYPE_CLASSIFICATION = 'classification';
    const TYPE_CLUSTERING = 'clustering';

    const TYPE_AGE = 'age';
    const TYPE_TIME = 'time_spend';

    private $method;
    private $columns;
    private $type;
    private $ageOrTime;
    private $dbQueryLimit;
    private $epsilon;
    private $minSamples;
    private $clusters;

    public function __construct()
    {
        $this->epsilon = 0.5;
        $this->minSamples = 3;
        $this->clusters = 3;
    }

    /**
     * @return float
     */
    public function getEpsilon()
    {
        return $this->epsilon;
    }

    /**
     * @param float $epsilon
     * @return $this
     */
    public function setEpsilon($epsilon)
    {
        $this->epsilon = $epsilon;

        return $this;
    }

    /**
     * @return int
     */
    public function getMinSamples()
    {
        return $this->minSamples;
    }

    /**
     * @param int $minSamples
     * @return $this
     */
    public function setMinSamples($minSamples)
    {
        $this->minSamples = $minSamples;

        return $this;
    }

    /**
     * @return int
     */
    public function getClusters()
    {
        return $this->clusters;
    }

    /**
     * @param int $clusters
     * @return $this
     */
    public function setClusters($clusters)
    {
        $this->clusters = $clusters;

        return $this;
    }

    /**
     * @return int
     */
    public function getDbQueryLimit()
    {
        return $this->dbQueryLimit;
    }

    /**
     * @param int $dbQueryLimit
     * @return $this
     */
    public function setDbQueryLimit($dbQueryLimit)
    {
        $this->dbQueryLimit = $dbQueryLimit;
        return $this;
    }

    /**
     * @param string $column
     * @return $this
     */
    public function addColumn($column)
    {
        $this->columns[] = $column;

        return $this;
    }

    /**
     * @return array
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * @param array $columns
     * @return $this
     */
    public function setColumns($columns)
    {
        $this->columns = $columns;

        return $this;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param string $methods
     * @return $this
     */
    public function setMethod($method)
    {
        $this->method = $method;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        if ($type !== self::TYPE_CLASSIFICATION && $type !== self::TYPE_CLUSTERING) {
            throw new InvalidArgumentException('Type is invalid');
        }

        $this->type = $type;

        return $this;
    }

    /**
     * @return int
     */
    public function getAgeOrTime()
    {
        return $this->ageOrTime;
    }

    /**
     * @param int $ageOrTime
     * @return $this
     */
    public function setAgeOrTime($ageOrTime)
    {
        $this->ageOrTime = $ageOrTime;

        return $this;
    }
}