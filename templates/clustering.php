<!DOCTYPE html>
<html>
<head>
    <title>Slide</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
<!-------------------------nav bar--------------------- -->
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-nav-demo" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="/" class="navbar-brand">Slidinėjimo trasų sistema</a>
        </div>
    </div>
</nav>

<div class="jumbotron text-center">
    <h1>Slidinėjimo trasų sistema</h1>
    <div class="row">
        <div class="col-lg-12 text-center">
            <div class="row">
                <form action="" method="GET">
                    <div class="col-lg-12"><h2>Klusterizavimo rezultatai</h2></div>
                    <div class="col-lg-4 col-lg-offset-4">
                        <table class="table table-border">
                            <tr>
                                <td>Vykdymo laikas</td>
                                <td><?php echo $result->getTimeTook(); ?> sekundžių (-ės)</td>
                            </tr>
                            <tr>
                                <td>Klusterių kiekis</td>
                                <td><?php echo count($result->getResults()); ?></td>
                            </tr>
                            <tr>
                                <td>Viso buvo paimta ir įrašų apmokymui</td>
                                <td><?php echo $result->getDataCount(); ?></td>
                            </tr>
                            <tr>
                                <td>Pasirinktas metodas</td>
                                <td><?php echo $request->getMethod(); ?></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <a href="/">Grįžti atgal</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12"><h2>Klusteriai ir jų duomenys</h2></div>
        <?php foreach ($result->getResults() as $cluster) { ?>
            <div class="col-lg-4">
                <table class="table table-bordered">
                    <tr>
                        <td colspan="<?php echo count($cluster[0]); ?>">Klusterio dydis</td>
                    </tr>
                    <tr>
                        <td colspan="<?php echo count($cluster[0]); ?>"><?php echo count($cluster); ?></td>
                    </tr>
                    <tr>
                        <?php foreach ($request->getColumns() as $method) { ?>
                            <th><?php echo brnck\DatabaseManager::mapColumnToValue($method); ?></th>
                        <?php } ?>
                    </tr>
                    <?php foreach ($cluster as $item) { ?>
                        <tr>
                            <?php foreach ($item as $key => $field) { ?>
                                <td><?php echo brnck\DatabaseManager::mapFieldToValue($request->getColumns()[$key], $field); ?></td>
                            <?php } ?>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        <? } ?>
    </div>
</div>

<script src="https://code.jquery.com/jquery-2.1.4.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>