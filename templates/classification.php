<!DOCTYPE html>
<html>
<head>
    <title>Slide</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
<!-------------------------nav bar--------------------- -->
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-nav-demo" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="/" class="navbar-brand">Slidinėjimo trasų sistema</a>
        </div>
    </div>
</nav>

<div class="jumbotron text-center">
    <h1>Slidinėjimo trasų sistema</h1>
    <div class="row">
        <div class="col-lg-12 text-center">
            <div class="row">
                <form action="" method="GET">
                    <div class="col-lg-12"><h2>Klasifikacijos rezultatai</h2></div>
                    <div class="col-lg-4 col-lg-offset-4">
                        <table class="table table-border">
                            <tr>
                                <td>Pasirinktas skaičius</td>
                                <td><?php echo $request->getAgeOrTime(); ?></td>
                            </tr>
                            <tr>
                                <td>Klasifikavimo rezultatas</td>
                                <td><?php echo ucfirst($result->getResults()[0]); ?></td>
                            </tr>
                            <tr>
                                <td>Vykdymo laikas</td>
                                <td><?php echo $result->getTimeTook(); ?> sekundžių (-ės)</td>
                            </tr>
                            <tr>
                                <td>Galimi klasifikavimo atsakymai</td>
                                <?php if($request->getColumns()[0] === brnck\Entity\Request::TYPE_AGE) { ?>
                                    <td>Paauglys, suaugęs, pensininkas</td>
                                <?php } else { ?>
                                    <td>Trumpai, vidutiniškai, ilgai</td>
                                <?php } ?>
                            </tr>
                            <tr>
                                <td>Viso buvo paimta ir įrašų apmokymui</td>
                                <td><?php echo $result->getDataCount(); ?></td>
                            </tr>
                            <tr>
                                <td>Pasirinktas metodas</td>
                                <td>K nearest neighbors</td>
                            </tr>
                            <tr>
                               <td colspan="2">
                                   <a href="/">Grįžti atgal</a>
                               </td>
                            </tr>
                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-2.1.4.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>