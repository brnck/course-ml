<!DOCTYPE html>
<html>
<head>
    <title>Slide</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
<!-------------------------nav bar--------------------- -->
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-nav-demo" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="/" class="navbar-brand">Slidinėjimo trasų sistema</a>
        </div>
    </div>
</nav>

<div class="jumbotron text-center">
    <h1>Slidinėjimo trasų sistema</h1>
    <div class="row">
        <div class="col-lg-12 text-center">
            <div class="row">
                <form action="" method="GET">
                    <div class="col-lg-12"><h2>Klusterizacija</h2></div>
                    <input type="hidden" value="request" name="request"/>
                    <div class="col-lg-3">
                        <h2>Duomenys</h2>
                        <div class=" form-group checkbox">
                            <label><input type="checkbox" name="columns[]" value="gender">Lytis</label>
                        </div>
                        <div class="checkbox form-group">
                            <label><input type="checkbox" name="columns[]" value="track">Trasa</label>
                        </div>
                        <div class="checkbox form-group">
                            <label><input type="checkbox" name="columns[]" value="age">Amžius</label>
                        </div>
                        <div class="checkbox form-group">
                            <label><input type="checkbox" name="columns[]" value="time_spend">Praleistas Laikas</label>
                        </div>
                        <div class="checkbox form-group">
                            <label><input type="checkbox" name="columns[]" value="equipment">Ar ėme įrangą</label>
                        </div>
                        <div class="checkbox form-group">
                            <label><input type="checkbox" name="columns[]" value="user">Lankytojai</label>
                        </div>

                    </div>
                    <div class="col-lg-2">
                        <h3>Duomenų kiekis</h3>
                        <div class="checkbox form-group">
                            <label><input type="number" name="limit" step="1" max="20000" min="1" value="1"/></label>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <h3>Konfiguracija</h3>
                        <h6>DBSCAN algoritmui</h6>
                        <div class="checkbox form-group">
                            <label><input type="number" name="epsilon" step="0.1" max="100" min="0.1" value="0.5"/>&nbsp;Epsilon</label>
                        </div>
                        <div class="checkbox form-group">
                            <label><input type="number" name="min_samples" step="1" max="20000" min="1" value="3"/>&nbsp;Minimalus kiekis</label>
                        </div>
                        <h6>K means algoritmui</h6>
                        <div class="checkbox form-group">
                            <label><input type="number" name="clusters" step="1" max="20000" min="1" value="3"/>Klusteriai</label>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <h2>Metodas</h2>
                        <div class="checkbox form-group">
                            <label><input type="radio" name="method" value="db_scan">&nbsp;Db scan</label>
                        </div>
                        <div class="checkbox form-group">
                            <label><input type="radio" name="method" value="k_means">&nbsp;K-means</label>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <input type="submit" class="btn btn-default" value="clustering" name="type">
                    </div>
                </form>
            </div>
        </div>
        <div class="col-lg-12 text-center">
            <div class="row">
                <form action="" method="GET">
                    <div class="col-lg-12"><h2>Klasifikacija</h2></div>
                    <input type="hidden" value="request" name="request"/>
                    <input type="hidden" name="type" value="classification" checked>
                    <div class="col-lg-3">
                        <h3>Duomenys</h3>
                        <div class="checkbox form-group">
                            <label><input type="radio" name="columns[]" value="age">&nbsp;Klasifikuoti pagal amžių</label>
                        </div>
                        <div class="checkbox form-group">
                            <label><input type="radio" name="columns[]" value="time_spend">&nbsp;Klasifikuoti pagal praleistą laiką</label>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <h3>Duomenų kiekis</h3>
                        <div class="checkbox form-group">
                            <label><input type="number" name="limit" step="1" max="20000" min="1" value="1"/></label>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <h3>Įveskite kliento amžių arba jo praleistą laiką</h3>
                        <div class="checkbox form-group">
                            <label><input type="number" name="ageOrTime" step="0.1"/></label>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <input type="submit" class="btn btn-default" name="type" value="classification" />
                    </div>
                </form>


            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-2.1.4.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>