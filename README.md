# Machine learning software
## Requirements
- PHP ^7.1
- Composer
- MySQL
- Optional:
  - Ability to use CLI if you are planning to run faker migrations

## Features
- Clustering
- Classification

## Run
- `cd path/to/project`
- `php -S localhost:8000`
- Open browser and go to `localhost:8000`